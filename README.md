# Geospatial Web

<http://www.geospatialweb.ca>

Development sandbox website to showcase the code integration of NestJS, React, Redux, ArcGIS API for JavaScript 4.14, Mapbox GL, Deck.GL, PostGIS 3.0 and Docker. This implementation features dedicated Map instances in separate routes.
