import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';

import { AppModule } from '../src/app.module';

describe('AppController', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule]
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('GET /api/mapbox-access-token', () => {
    return request(app.getHttpServer())
      .get('/api/mapbox-access-token')
      .expect(200)
      .expect(JSON.stringify(process.env.ACCESSTOKEN));
  });
});
