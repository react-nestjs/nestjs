/* original AuthGuard before implementing Passport-Jwt AuthGuard
 * no longer actively used but left in repo to document custom guard construction
 * refer to auth/middlewares/auth.middleware.ts
 */
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const req = context.switchToHttp().getRequest();
    return this.validateRequest(req);
  }

  validateRequest(req: Request): boolean {
    return (req as any).isAuth;
  }
}
