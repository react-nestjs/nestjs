import { Injectable } from '@nestjs/common';
import { Pool, QueryResult } from 'pg';

import { QueryParamsDto } from '../dtos/query-params.dto';

@Injectable()
export class ApiService {
  getGeojson(params: QueryParamsDto): Promise<[{ geojson: string }]> {
    const { DATABASE_URI } = process.env;
    // const { DATABASE_URI_LOCAL } = process.env;

    const query: string = `
      SELECT ST_AsGeoJSON(feature.*) AS geojson
      FROM (
        SELECT ${params.fields}
        FROM ${params.table}
      ) AS feature
    `;

    const pool: Pool = new Pool({
      /* docker instance: DATABASE_URI */
      /* local instance:  DATABASE_URI_LOCAL */
      connectionString: DATABASE_URI
    }).on('error', err => {
      console.error('Connection Failed:\n', err);
      process.exit(1);
    });

    return new Promise((resolve: any): void => {
      pool
        .query(query)
        .then(({ rows }: QueryResult): void => resolve(rows))
        .catch((err: Error): void => console.error('Query Failed:\n', err));
    });
  }

  getMapboxAccessToken(): string {
    const { MAPBOX_ACCESS_TOKEN } = process.env;
    return JSON.stringify(MAPBOX_ACCESS_TOKEN);
  }
}
