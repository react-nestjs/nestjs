import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';

import { LocalStrategy } from '../../auth/strategies/local.strategy';

@Injectable()
export class ApiMiddleware implements NestMiddleware {
  constructor(private readonly localStrategy: LocalStrategy) {}

  use(req: Request, _: Response, next: NextFunction) {
    if (this.localStrategy.isAuthenticated) {
      const { method, url } = req;

      (req as any).isAuth = this.localStrategy.isAuthenticated;
      console.log(`${method}  ${url}: ${(req as any).isAuth.toString().toUpperCase()}`);
    }

    next();
  }
}
