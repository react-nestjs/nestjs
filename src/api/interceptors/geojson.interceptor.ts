/* transforms raw query output ('result.rows') returned via '/api/geojson' route into a GeoJSON Feature Collection
 * en route back to originating front-end GET request
 */
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { FeatureCollection } from 'geojson';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GeojsonService } from '../services/geojson/geojson.service';

@Injectable()
export class GeojsonInterceptor implements NestInterceptor {
  constructor(private readonly geojsonService: GeojsonService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<FeatureCollection> {
    return next
      .handle()
      .pipe(
        map(
          (features: [{ geojson: string }]): FeatureCollection =>
            this.geojsonService.createFeatureCollection(features)
        )
      );
  }
}
