import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';

import { RateLimiterService } from '../services/rateLimiter/rate-limiter.service';

@Injectable()
export class RateLimiterMiddleware implements NestMiddleware {
  constructor(private readonly rateLimiterService: RateLimiterService) {}

  use(req: Request, res: Response, next: NextFunction): void {
    this.rateLimiterService.rateLimiter(req, res, next);
  }
}
