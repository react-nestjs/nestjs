import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { DataService } from '../services/data/data.service';

@UseGuards(AuthGuard('jwt'))
@Controller('data')
export class DataController {
  constructor(private readonly dataService: DataService) {}

  @Get('events/daily')
  async getEventsDaily(): Promise<[]> {
    return await this.dataService.getEventsDaily();
  }

  @Get('events/daily/poi')
  async getEventsDailyPOI(): Promise<[]> {
    return await this.dataService.getEventsDailyPOI();
  }

  @Get('events/hourly')
  async getEventsHourly(): Promise<[]> {
    return await this.dataService.getEventsHourly();
  }

  @Get('events/hourly/poi')
  async getEventsHourlyPOI(): Promise<[]> {
    return await this.dataService.getEventsHourlyPOI();
  }

  @Get('stats/daily')
  async getStatsDaily(): Promise<[]> {
    return await this.dataService.getStatsDaily();
  }

  @Get('stats/daily/poi')
  async getStatsDailyPOI(): Promise<[]> {
    return await this.dataService.getStatsDailyPOI();
  }

  @Get('stats/hourly')
  async getStatsHourly(): Promise<[]> {
    return await this.dataService.getStatsHourly();
  }

  @Get('stats/hourly/poi')
  async getStatsHourlyPOI(): Promise<[]> {
    return await this.dataService.getStatsHourlyPOI();
  }
}
