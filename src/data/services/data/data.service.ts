import { Injectable } from '@nestjs/common';
import { Pool, QueryResult } from 'pg';

@Injectable()
export class DataService {
  query: string;

  connection: { user: string; host: string; database: string; password: string; port: number } = {
    user: process.env.PGUSER,
    host: process.env.PGHOST,
    database: process.env.PGDATABASE,
    password: process.env.PGPASSWORD,
    port: +process.env.PGPORT
  };

  pool: Pool = new Pool(this.connection).on('error', (err: Error) => {
    console.error('Connection Failed:\n', err);
    process.exit(1);
  });

  queryHandler(): Promise<[]> {
    return new Promise((resolve: any): void => {
      this.pool
        .query(this.query)
        .then(({ rows }: QueryResult): void => resolve(rows || []))
        .catch((err: Error): void => console.error('Query Failed:\n', err));
    });
  }

  getEventsDaily(): Promise<[]> {
    this.query = `
      SELECT date, SUM(events)::INTEGER AS events
      FROM public.hourly_events
      GROUP BY date
      ORDER BY date
      LIMIT 7;
    `;

    return this.queryHandler();
  }

  getEventsDailyPOI(): Promise<[]> {
    this.query = `
    SELECT
      ROW_NUMBER() OVER (ORDER BY date, public.poi.name)::INTEGER AS id,
      date,
      public.poi.name,
      SUM(events)::INTEGER AS events,
      public.poi.lat,
      public.poi.lon
    FROM public.hourly_events
    JOIN public.poi ON public.hourly_events.poi_id=public.poi.poi_id
    WHERE date <= '2017-01-31'
    GROUP BY date, public.poi.name, public.poi.lat, public.poi.lon
    ORDER BY date, public.poi.name
  `;

    return this.queryHandler();
  }

  getEventsHourly(): Promise<[]> {
    this.query = `
      SELECT date, hour, events
      FROM public.hourly_events
      ORDER BY date, hour
      LIMIT 168;
    `;

    return this.queryHandler();
  }

  getEventsHourlyPOI(): Promise<[]> {
    this.query = `
      SELECT
        ROW_NUMBER() OVER (ORDER BY date, hour)::INTEGER AS id,
        date,
        hour,
        public.poi.name,
        events,
        public.poi.lat,
        public.poi.lon
      FROM public.hourly_events
      JOIN public.poi ON public.hourly_events.poi_id=public.poi.poi_id
      WHERE date <= '2017-01-31'
      ORDER BY date, hour
  `;

    return this.queryHandler();
  }

  getStatsDaily(): Promise<[]> {
    this.query = `
      SELECT
        date,
        SUM(impressions)::INTEGER AS impressions,
        SUM(clicks)::INTEGER AS clicks,
        SUM(revenue)::DOUBLE PRECISION AS revenue
      FROM public.hourly_stats
      GROUP BY date
      ORDER BY date
      LIMIT 7;
    `;

    return this.queryHandler();
  }

  getStatsDailyPOI(): Promise<[]> {
    this.query = `
      SELECT
        ROW_NUMBER() OVER (ORDER BY date, public.poi.name)::INTEGER AS id,
        date,
        public.poi.name,
        SUM(impressions)::INTEGER AS impressions,
        SUM(clicks)::INTEGER AS clicks,
        SUM(revenue)::DOUBLE PRECISION AS revenue,
        public.poi.lat,
        public.poi.lon
      FROM public.hourly_stats
      JOIN public.poi ON public.hourly_stats.poi_id=public.poi.poi_id
      WHERE date <= '2017-01-31'
      GROUP BY date, public.poi.name, public.poi.lat, public.poi.lon
      ORDER BY date, public.poi.name
    `;

    return this.queryHandler();
  }

  getStatsHourly(): Promise<[]> {
    this.query = `
      SELECT date, hour, impressions, clicks, revenue::DOUBLE PRECISION
      FROM public.hourly_stats
      ORDER BY date, hour
      LIMIT 168;
    `;

    return this.queryHandler();
  }

  getStatsHourlyPOI(): Promise<[]> {
    this.query = `
      SELECT
        ROW_NUMBER() OVER (ORDER BY date, hour)::INTEGER AS id,
        date,
        hour,
        public.poi.name,
        impressions,
        clicks,
        revenue::DOUBLE PRECISION,
        public.poi.lat,
        public.poi.lon
      FROM public.hourly_stats
      JOIN public.poi ON public.hourly_stats.poi_id=public.poi.poi_id
      WHERE date <= '2017-01-31'
      ORDER BY date, hour
    `;

    return this.queryHandler();
  }
}
