import { Injectable } from '@nestjs/common';
import { Request, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';

import * as moment from 'moment';
import { RedisClient } from 'redis';

const redis = require('redis');

@Injectable()
export class RateLimiterService {
  private client: RedisClient = null;
  private key: string = null;
  private REQUESTS_THRESHOLD: number = 10;

  private createRequest(requests: any[]): void {
    let request: { expiryTime: moment.Moment } = null;

    if (!requests) {
      requests = [];
      request = {
        expiryTime: moment().add(30, 'seconds')
      };
    }

    if (requests.length) {
      request = {
        expiryTime: requests[0].expiryTime
      };
    }

    requests.push(request);
    this.client.set(this.key, JSON.stringify(requests));
  }

  private hasTimeLimitExpired(expiryTime: moment.Moment): boolean {
    return moment().isAfter(expiryTime);
  }

  async rateLimiter(req: Request, res: Response, next: NextFunction): Promise<void> {
    this.key = req.headers.authorization.split(' ')[1].split('.')[0];

    if (!this.client) {
      this.client = await redis.createClient({
        host: 'redis-19152.c114.us-east-1-4.ec2.cloud.redislabs.com',
        password: process.env.REDIS_PASSWORD,
        port: 19152
      });
    }

    this.client.auth(process.env.REDIS_PASSWORD, (err: Error) => {
      if (err) {
        console.error(`Redis Client Authentication Error: ${err}`);
        throw err;
      }
    });

    this.client.exists(this.key, (err: Error, keyExists: number): void => {
      if (err) {
        console.error(`Redis Response Error: ${err}`);
        process.exit(1);
      }

      if (!keyExists) {
        this.createRequest(null);
        return next();
      } else {
        this.client.get(this.key, (err: Error, payload: string) => {
          if (err) {
            console.error(`Redis Payload Error: ${err}`);
            throw err;
          }

          const requests: any[] = JSON.parse(payload);
          const expiryTime: moment.Moment = moment(requests[0].expiryTime);

          if (!this.hasTimeLimitExpired(expiryTime)) {
            if (requests.length === this.REQUESTS_THRESHOLD) {
              const currentTime: moment.Moment = moment();
              const waitTime: number = moment.duration(expiryTime.diff(currentTime)).asSeconds();

              return res.status(429).send(
                `Exceeded API Rate Limit
                Please wait ${Math.round(waitTime)} seconds to make next request`
              );
            } else {
              this.createRequest(requests);
              return next();
            }
          } else {
            this.client.del(this.key);
            this.createRequest(null);
            return next();
          }
        });
      }
    });
  }
}
