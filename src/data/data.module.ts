import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { DataController } from './controllers/data.controller';
import { DataService } from './services/data/data.service';
import { RateLimiterMiddleware } from './middlewares/rate-limiter.middleware';
import { RateLimiterService } from './services/rateLimiter/rate-limiter.service';

@Module({
  controllers: [DataController],
  providers: [DataService, RateLimiterService]
})
export class DataModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RateLimiterMiddleware).forRoutes(DataController);
  }
}
