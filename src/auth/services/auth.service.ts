import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { JwtAccessToken } from '../types/jwt-access-token';
import { User } from '../../user/entities/user.entity';

import { UserService } from '../../user/services/user.service';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService, private readonly userService: UserService) {}

  login({ id, username }: User): JwtAccessToken {
    return {
      accessToken: this.jwtService.sign({ sub: id, username }),
      expiresIn: 3600
    };
  }

  async register(user: User): Promise<User> {
    return await this.userService.create(user);
  }

  async validateUser(username: string, password: string): Promise<User | null> {
    const user: User = await this.userService.findByUsername(username);

    if (user && user.password === password) {
      Reflect.deleteProperty(user, 'password');
      return user;
    }

    return null;
  }
}
