import { Controller, Body, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { JwtAccessToken } from '../types/jwt-access-token';
import { User } from '../../user/entities/user.entity';

import { AuthService } from '../services/auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  login(@Request() req: any): JwtAccessToken {
    return this.authService.login(req.user as User);
  }

  @Post('register')
  async register(@Body() user: User): Promise<User> {
    return await this.authService.register(user);
  }
}
